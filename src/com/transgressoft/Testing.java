package com.transgressoft;

import com.transgressoft.timecode.FrameRateType;
import com.transgressoft.timecode.Timecode;
import com.transgressoft.timecode.df30.Df30Timecode;
import com.transgressoft.timecode.fps24.Fps24Timecode;
import com.transgressoft.timecode.fps24.Fps24TimecodeFactory;

public class Testing {

	public static void main(String[] args) {
		
		//compare 29.97
		System.out.println("comparison 29.97");
		System.out.println(Df30Timecode.of(1800));
		oldTimeCode tc1 = new oldTimeCode(1800, 29.97);
		System.out.println(tc1.notation());
		
		
		System.out.println("\n\n\n");
		
		System.out.println("comparison 23.976");
//		int hours = 1;
//		int minutes = 0;
//		int seconds = 0;
//		int frames = 0;

		Timecode timeCode24 = Fps24TimecodeFactory.createTimeCode(FrameRateType.FPS24, 86400);

//		System.out.println("new TC class: " + notation(timeCode24));
		System.out.println(Fps24Timecode.of(86400));
		oldTimeCode tc2 = new oldTimeCode(86400, 23.976);
		System.out.println("old TC class: " + tc2.notation());

	}

	private static String notation(Timecode timeCode24) {
		return ((timeCode24.getHours() < 10) ? "0" + timeCode24.getHours() : timeCode24.getHours()) + ":"
				+ ((timeCode24.getMinutes() < 10) ? "0" + timeCode24.getMinutes() : timeCode24.getMinutes()) + ":"
				+ ((timeCode24.getSeconds() < 10) ? "0" + timeCode24.getSeconds() : timeCode24.getSeconds()) + ":"
				+ ((timeCode24.getFrames() < 10) ? "0" + timeCode24.getFrames() : timeCode24.getFrames());
	}

}

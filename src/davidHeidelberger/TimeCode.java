package davidHeidelberger;

public class TimeCode {

	private double framerate;

	public TimeCode(double framerate) {
		this.framerate = framerate;
	}

//	Frame Number to Drop-Frame Timecode

	public String framesToDf(double framenumber) {
		// CONVERT A FRAME NUMBER TO DROP FRAME TIMECODE
		// Code by David Heidelberger, adapted from Andrew Duncan
		// Given an int called framenumber and a double called framerate
		// Framerate should be 29.97 or 59.94, otherwise the calculations will be off.

		int d;
		int m;
		String res = "";

		int dropFrames = (int) Math.round(framerate * .066666); // Number of frames to drop on the minute marks is the
																// nearest integer to 6% of the framerate
		int framesPerHour = (int) Math.round(framerate * 60 * 60); // Number of frames in an hour
		int framesPer24Hours = framesPerHour * 24; // Number of frames in a day - timecode rolls over after 24 hours
		int framesPer10Minutes = (int) Math.round(framerate * 60 * 10); // Number of frames per ten minutes
		int framesPerMinute = ((int) (Math.round(framerate) * 60) - dropFrames); // Number of frames per minute is the
																					// round of the framerate * 60 minus
																					// the number of dropped frames

		// In some languages, a % operation will work here
		// But since % for negative numbers varies by language, we'll do it manually
		while (framenumber < 0) // Negative time. Add 24 hours.
		{
			framenumber = framesPer24Hours + framenumber;
		}

		// If framenumber is greater than 24 hrs, next operation will rollover clock
		framenumber = framenumber % framesPer24Hours; // % is the modulus operator, which returns a remainder. a % b =
														// the remainder of a/b

		d = (int) framenumber / framesPer10Minutes; // \ means integer division, which is a/b without a remainder. Some
													// languages you could use floor(a/b)
		m = (int) framenumber % framesPer10Minutes;

		// In the original post, the next line read m>1, which only worked for 29.97.
		// Jean-Baptiste Mardelle correctly pointed out that m should be compared to
		// dropFrames.
		if (m > dropFrames) {
			framenumber = framenumber + (dropFrames * 9 * d) + dropFrames * ((m - dropFrames) / framesPerMinute);
		} else {
			framenumber = framenumber + dropFrames * 9 * d;
		}

		int frRound = (int) Math.round(framerate);
		int frames = (int) (framenumber % frRound);
		int seconds = (int) ((framenumber / frRound) % 60);
		int minutes = (int) (((framenumber / frRound) / 60) % 60);
		int hours = (int) (((framenumber / frRound) / 60) / 60);

		res += (hours) + ";";
		res += (minutes) + ";";
		res += (seconds) + ";";
		res += (frames);
		return res;
	}

//	Drop-Frame Timecode to Frame Number
	public int DfToFrames(String timecode) {
		// CONVERT DROP FRAME TIMECODE TO A FRAME NUMBER
		// Code by David Heidelberger, adapted from Andrew Duncan
		// Given ints called hours, minutes, seconds, frames, and a double called
		// framerate
		if (timecode.contains(":")) {
			timecode = timecode.replace(":", ";");
		}

		String[] split = timecode.split(";");
		if (split.length < 4) {
			return -1;
		}

		int hours = Integer.parseInt(split[0]);
		int minutes = Integer.parseInt(split[1]);
		int seconds = Integer.parseInt(split[2]);
		int frames = Integer.parseInt(split[3]);

		// CONVERT DROP FRAME TIMECODE TO A FRAME NUMBER
		// Code by David Heidelberger, adapted from Andrew Duncan
		// Given ints called hours, minutes, seconds, frames, and a double called
		// framerate

		int dropFrames = (int) Math.round(framerate * 0.066666); // Number of drop frames is 6% of framerate rounded to
																	// nearest integer
		int timeBase = (int) Math.round(framerate); // We don't need the exact framerate anymore, we just need it
													// rounded to nearest integer

		int hourFrames = timeBase * 60 * 60; // Number of frames per hour (non-drop)
		int minuteFrames = timeBase * 60; // Number of frames per minute (non-drop)
		int totalMinutes = (60 * hours) + minutes; // Total number of minuts
		int frameNumber = ((hourFrames * hours) + (minuteFrames * minutes) + (timeBase * seconds) + frames)
				- (dropFrames * (totalMinutes - (totalMinutes / 10)));
		return frameNumber;
	}

//	Frame Number to NDF
	public String framesToNdf(int framenumber) {
		// CONVERT A FRAME NUMBER TO NON-DROP-FRAME TIMECODE
		// Code by David Heidelberger
		// Given an int called framenumber and a double called framerate

		String res = "";

		int timeBase = (int) Math.round(framerate); // We don't need the exact framerate, we just need it rounded to
													// nearest integer

		int framesPerHour = timeBase * 60 * 60; // Number of frames in an hour
		int framesPer24Hours = framesPerHour * 24; // Number of frames in a day - timecode rolls over after 24 hours

		// In some languages, a % operation will work here
		// But since % for negative numbers varies by language, we'll do it manually
		while (framenumber < 0) // Negative time. Add 24 hours.
		{
			framenumber = framenumber + framesPer24Hours;
		}

		// If framenumber is greater than 24 hrs, next operation will rollover clock
		framenumber = framenumber % framesPer24Hours; // % is the modulus operator, which returns a remainder. a % b =
														// the remainder of a / b

		int remainingFrames = framenumber; // We'll keep subtracting frames from this as we go

		int hourFrames = timeBase * 60 * 60; // Number of frames per hour (non-drop)
		int minuteFrames = timeBase * 60; // Number of frames per minute (non-drop)

		// Do integer division for hours, minutes, seconds and then subtract from
		// remaining frames
		int hours = remainingFrames / hourFrames;
		remainingFrames = remainingFrames - (hours * hourFrames);

		int minutes = remainingFrames / minuteFrames;
		remainingFrames = remainingFrames - (minutes * minuteFrames);

		int seconds = remainingFrames / timeBase;
		int frames = remainingFrames - (seconds * timeBase);

		res += (hours) + ":";
		res += (minutes) + ":";
		res += (seconds) + ":";
		res += (frames);
		return res;
	}

//	NDF to Frame Number
	public int NdfToFrames(String timecode) {
		// CONVERT NON-DROP-FRAME TIMECODE TO A FRAME NUMBER
		// Code by David Heidelberger
		// Given ints called hours, minutes, seconds, frames, and a double called
		// framerate

		if (timecode.contains(";")) {
			timecode = timecode.replace(";", ":");
		}

		String[] split = timecode.split(":");
		if (split.length < 4) {
			return -1;
		}

		int hours = Integer.parseInt(split[0]);
		int minutes = Integer.parseInt(split[1]);
		int seconds = Integer.parseInt(split[2]);
		int frames = Integer.parseInt(split[3]);

		int timeBase = (int) Math.round(framerate); // We don't need the exact framerate, we just need it rounded to
													// nearest
		// integer

		int hourFrames = timeBase * 60 * 60; // Number of frames per hour (non-drop)
		int minuteFrames = timeBase * 60; // Number of frames per minute (non-drop)

		int frameNumber = (hourFrames * hours) + (minuteFrames * minutes) + (timeBase * seconds) + frames;

		return frameNumber;
	}

//	Frame Number to Pseudo-Drop-Frame 24p
	public String framesToDf24p(int framenumber) {
		// CONVERT A FRAME NUMBER TO PSEUDO-DROP-FRAME 24P TIMECODE
		// Code by David Heidelberger
		// Given an int called framenumber and a double called framerate (which should
		// be 23.976 in this case)

		String res = "";

		double secondsDecimal = framenumber / framerate;

		// We only want the integer part of secondsDecimal, we'll deal with the decimal
		// part next
		int secondsInt = (int) Math.floor(secondsDecimal); // Round down to nearest integer

		// Now we need to convert the decimal part of secondsDecimal to frames
		// Different languages do this differently, but a generally reasonable strategy
		// would be:
		double secondsDecimalPart = secondsDecimal - secondsInt;
		int frames = (int) Math.round(secondsDecimalPart * framerate);

		// If rounding the frames gives us an extra second, account for it here
		if (frames == 24) {
			frames = 0;
			secondsInt = secondsInt + 1;
		}

		int remainingSeconds = secondsInt; // We'll keep subtracting from this as we go

		// Do integer division for hours and minutes and then subtract from remaining
		// seconds
		int hours = remainingSeconds / (60 * 60);
		remainingSeconds = remainingSeconds - (hours * 60 * 60);

		int minutes = remainingSeconds / 60;
		remainingSeconds = remainingSeconds - (minutes * 60);

		int seconds = remainingSeconds;

		res += (hours) + ";";
		res += (minutes) + ";";
		res += (seconds) + ";";
		res += (frames);
		return res;
	}

//	Pseudo-Drop-Frame 24p to Frame Number
	public int df24pToFrames(String timecode) {
		// CONVERT PSEUDO-DROP-FRAME 24P TIMECODE TO A FRAME NUMBER
		// Code by David Heidelberger
		// Given ints called hours, minutes, seconds, frames, and a double called
		// framerate (which should be 23.976 in this case)
		if (timecode.contains(":")) {
			timecode = timecode.replace(":", ";");
		}

		String[] split = timecode.split(";");
		if (split.length < 4) {
			return -1;
		}

		int hours = Integer.parseInt(split[0]);
		int minutes = Integer.parseInt(split[1]);
		int seconds = Integer.parseInt(split[2]);
		int frames = Integer.parseInt(split[3]);

		double hourFrames = framerate * 60 * 60; // Number of frames per hour (non-drop)
		double minuteFrames = framerate * 60; // Number of frames per minute (non-drop)

		// Just multiply it all out and then round
		double framesDouble = (hours * hourFrames) + (minutes * minuteFrames) + (seconds * framerate) + frames;

		int frameNumber = (int) Math.round(framesDouble);
		return frameNumber;
	}

}
